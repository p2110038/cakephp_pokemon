<div align="center">

# Pokeshop Application Skeleton
![](webroot/pika_icone.ico)
</div>

## Installation

 Cloner le dépôt par le GIT

  Dans votre terminal,vous devez effectué les opérations suivantes :

- En premier tapez **composer install** dans votre terminal suivi d'un **composer update**
```bash
**composer install** puis **composer update**
```
- Puis installez Bootstap par la commande **npm install bootstrap@3**

```bash
**npm install bootstrap@3**
```

- Une fois tous ceci fais toujours dans votre terminal faite un bin/cake server -p 8765 pour lancer votre serveur
        Si cela ne marche pas faite un **cd \bin\** puis terminer par un **.\cake server** pour lancer votre serveur
```bash
bin/cake server -p 8765
```

- Votre serveur est maintenant démarré est accesible via votre navigateur internet
        Son adresse URL est stipulé dans le terminal mais devrait être http://localhost:8765

### Utilisation de l'API
L'utilisation de l'API se fait avec un rajout de /api à la fin de l'URL.


```bash
 /api
```
Renvoie la liste complète des pokémons disponibles dans la base au format JSON.

```bash
/api/cards/*type_pokémon*
```
Renverra un document avec tous les pokémons ayant le même type que celui spécifié dans l'URL en format JSON.

```bash
/api/cards/name/*name_poké*
```
renvoie le champs correspond au pokémon si celui ci est présent dans la base, le nom peut-être en majuscule, ou en minuscule en format JSON.


