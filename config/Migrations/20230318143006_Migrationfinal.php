<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Migrationfinal extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up(): void
    {
        $this->table('Dept', ['id' => false])
            ->addColumn('departement', 'string', [
                'default' => null,
                'limit' => 3,
                'null' => true,
            ])
            ->addColumn('nbVilles', 'biginteger', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('superficie', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('altitudeMax', 'smallinteger', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('altitudeMin', 'smallinteger', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('sum(pop2010)', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 30,
                'scale' => 0,
            ])
            ->create();

        $this->table('EMP_NOTTHRIL', ['id' => false])
            ->addColumn('idemprunteur', 'string', [
                'default' => null,
                'limit' => 3,
                'null' => false,
            ])
            ->addColumn('ville', 'string', [
                'default' => null,
                'limit' => 6,
                'null' => true,
            ])
            ->create();

        $this->table('IUT20222023', ['id' => false])
            ->addColumn('ï»¿IND -Cod Sex', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Lib Sit Fam', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('DateNaissance', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Ville Nais', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Lic DEP+PAYS Nais', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -CODE DEP PAY NAI', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Lib NationalitÃ©', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IBA -AnnÃ©e Bac', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('BAC -Lic Bac', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IBA- SÃ©rie Bac', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('BAC -Lib Bac', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('BAC -Lib Mention', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IBA -Cod Etb du Bac', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETB OBT-Lib type etab', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETB OBT-Lib long etab', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('DPT ETB OBT-Lib Dept Etb', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAE -Code Ã©tape', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAE -Cod Vrs Etp', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('Lic Etp', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETP LibellÃ© Ã©tape', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAE -Nbr Ins dans Dip', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAE -Nbr Ins dans Etp', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code Sport HN', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Lib Sport HN', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Cod Typ handicap', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Lib Type Handicap', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRA -Code BDI en cours', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRA -Lib Ach en cours', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRA -Lib Pays en cours', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRF -Code Bdi fixe', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRF -Lib Ach fixe', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ADRF -Lib Pays fixe', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IND -Cod Nni Etu', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Type dip sis Ann Pre', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('TDS -Lib typ dip Sise Ann Pre', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETB ANT-Code etab', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETB ANT-Lib Type etab', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ETB ANT-Lib long etab', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('DPT ETB ANT-Lib Dept Etb', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IBA -DÃ©pt Bac', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code CatÃ©gorie Socio-Professionnelle Parent', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -LibellÃ© CatÃ©gorie Socio-Professionnelle Parent', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code CatÃ©gorie Socio-Professionnelle Autre Parent', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -LibellÃ© CatÃ©gorie Socio-Professionnelle Autre Parent', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code Cat Soc prof Etu', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Lib Cat Socio Prof Etu', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Tem boursier', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAE -Code boursier', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('BRS -Lic Bourse', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('BRS -Ech Bourse', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code RÃ©gime ins', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Lib RÃ©gime Ins', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Lic RÃ©gime ins', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Code Statut', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('IAA -Lib Statut ins', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('cards')
            ->addColumn('namePokemon', 'string', [
                'default' => null,
                'limit' => 22,
                'null' => false,
            ])
            ->addColumn('type1Pokemon', 'string', [
                'default' => null,
                'limit' => 22,
                'null' => false,
            ])
            ->addColumn('type2Pokemon', 'string', [
                'default' => null,
                'limit' => 22,
                'null' => true,
            ])
            ->addColumn('urlPokemon', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('stock', 'integer', [
                'default' => '1',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('prix', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('emprunts', ['id' => false, 'primary_key' => ['livre', 'dateEmp', 'lecteur']])
            ->addColumn('livre', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('dateEmp', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('lecteur', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('dateRet', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'livre',
                ]
            )
            ->addIndex(
                [
                    'lecteur',
                ]
            )
            ->create();

        $this->table('lecteurs', ['id' => false, 'primary_key' => ['numLecteur']])
            ->addColumn('numLecteur', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('nom', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('prenom', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => true,
            ])
            ->addColumn('adr', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('tel', 'char', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('genre', 'char', [
                'default' => null,
                'limit' => 1,
                'null' => true,
            ])
            ->addColumn('montant', 'integer', [
                'default' => '0',
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('livres', ['id' => false, 'primary_key' => ['numLivre']])
            ->addColumn('numLivre', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('titre', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('auteur', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->create();

        $this->table('rencontre', ['id' => false])
            ->addColumn('l1', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('l2', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('users')
            ->addColumn('username', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => true,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => true,
            ])
            ->create();

        $this->table('emprunts')
            ->addForeignKey(
                'livre',
                'livres',
                'numLivre',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT',
                ]
            )
            ->addForeignKey(
                'lecteur',
                'lecteurs',
                'numLecteur',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT',
                ]
            )
            ->update();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down(): void
    {
        $this->table('emprunts')
            ->dropForeignKey(
                'livre'
            )
            ->dropForeignKey(
                'lecteur'
            )->save();

        $this->table('Dept')->drop()->save();
        $this->table('EMP_NOTTHRIL')->drop()->save();
        $this->table('IUT20222023')->drop()->save();
        $this->table('cards')->drop()->save();
        $this->table('emprunts')->drop()->save();
        $this->table('lecteurs')->drop()->save();
        $this->table('livres')->drop()->save();
        $this->table('rencontre')->drop()->save();
        $this->table('users')->drop()->save();
    }
}
